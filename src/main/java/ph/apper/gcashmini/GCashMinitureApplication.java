package ph.apper.gcashmini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GCashMinitureApplication {

	public static void main(String[] args) {
		SpringApplication.run(GCashMinitureApplication.class, args);
	}

}
